# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [2.0.3] - 13/01/2021
- Update module to be D9-ready

## [2.0.2] - 4/12/2020
- Add composer.json file

## [2.0.1] - 18/07/2018
- Fixed issue of on uninstall module page

## [2.0.0] 
- Initial version of Drupal 8 module