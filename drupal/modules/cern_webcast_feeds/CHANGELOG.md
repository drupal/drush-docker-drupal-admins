# Changelog
All notable changes to this project will be documented in this file.

The format is based on Keep a Changelog, and this project adheres to Semantic Versioning.

## [2.0.1] - 15/01/2021
- Add composer.json file
- Update module to be D9-ready
- Move files one level higher
