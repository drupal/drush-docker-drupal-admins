#!/bin/sh

#This script will be the first thing executed by users to finalize the preparation of the environment
set -x

#Cleanup previous executions

umount /mnt/site
rm -rf /mnt/site
rm -rf /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/


#mount the webdav site's folder
mkdir -p /mnt/site
mount -t davfs https://${DRUSH_SITENAME}/_webdav /mnt/site || exit

#prepare the drushrc
envsubst '$DRUSH_SITENAME$DRUPAL_VERSION' < /drush/drushrc.php > /etc/drush/drushrc.php

# create folder for the site
mkdir -p /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/

#symlink all webdav folders to the final destination
find /mnt/site/ -mindepth 1 -maxdepth 1 -type d -exec ln -s {} /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/ \;

#copy settings.php to the final destination
envsubst '$DRUSH_SITENAME$DRUSH_HOST$DRUSH_EGROUP$DRUSH_OWNER$DRUSH_EGROUP$DRUSH_USERNAME$DRUSH_PASSWORD$DRUSH_DATABASE$DRUSH_PORT$DRUPAL_VERSION' < /drush/settings.php > /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/settings.php

rm /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/private/.docker.rerun.flag

cd /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/
