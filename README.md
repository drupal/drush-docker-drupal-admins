# drush-docker-drupal-admins

Docker image containing all the neccesary to allow administrators of a site to run drush commands on their Drupal 8 website

## Sync drupal code base from production

```
rsync -av --copy-links --delete --include '/sites/sites.php' --exclude sites --exclude .git root@drupal8p24:/mnt/data/drupal/8.X.Y/ ./drupal/
```

Update version in Dockerfile

## Update image

Two actions: sync code with previous command and change DRUPAL_VERSION variable on Dockerfile

- Commit changes to master

Sometime you can have permissions problems like:

```
[eduardoa@aiadm02:~/code/drush-docker-drupal-admins] (master)# git add --all
error: open("drupal/INSTALL.txt"): Permission denied
error: unable to index file drupal/INSTALL.txt
fatal: updating files failed
```

You can resolve them by executing

```
cd git_repo_dir
find ./drupal/ ! -readable -exec chmod u+r {} \;
```

- Wait for the latest image to get built
- Test

On any D8 test site, follow /_site/drush.php Change stable with latest to test the latest version.
Run any drush operation like 
```
drush cr
```

- Push the change to stable, for that
    - Tag the commit
    - Execute the manual pipeline to create stable image

